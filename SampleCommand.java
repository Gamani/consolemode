package com.example.examplemod;

import java.util.ArrayList;
import java.util.List;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.world.WorldServer;
import net.minecraft.world.World;
import net.minecraft.util.ChatComponentText;
import net.minecraft.server.MinecraftServer;
import net.minecraft.item.ItemStack;
import net.minecraft.entity.player.EntityPlayer;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

public class SampleCommand implements ICommand



{
	{ System.out.println("TEST ECHO FROM ANOTHER CLASS"); }
  private List aliases;
  public SampleCommand()
  {
    this.aliases = new ArrayList();
    this.aliases.add("consolemod");
    this.aliases.add("/consolemod");
    this.aliases.add("conmod");
    this.aliases.add("/conmod");
  }

  @Override
  public String getCommandName()
  {
    return "Consolemodz";
  }

  @Override
  public String getCommandUsage(ICommandSender icommandsender)
  {
    return "ConsoleMod version 0.1 alpha"
    		+ "Written by GamaniaTV"
    		+ "To work mod, you need to contact me"
    		+ "Usage:"
    		+ "/consolemod on - toggle consolemod to send commands from WebConsole"
    		+ "/consolemod off - toggle consolemod to stop send and recive commands from WebConsole"
    		+ "/consolemod checkserver - check server work (online or offline)"
    		+ "Aliases: /consolemod //consolemod /conmod //conmod";
  }

  @Override
  public List getCommandAliases()
  {
    return this.aliases;
  }

  @Override
  public void processCommand(ICommandSender icommandsender, String[] astring)
  {
    if(astring.length == 0)
    {
    	icommandsender.addChatMessage(new ChatComponentText("\247eConsoleMod version 0.1 alpha"));
    	icommandsender.addChatMessage(new ChatComponentText("\2475Written by GamaniaTV"));
    	icommandsender.addChatMessage(new ChatComponentText("\247eTo work mod, you need to contact me"));
    	icommandsender.addChatMessage(new ChatComponentText("\247eUsage:"));
    	icommandsender.addChatMessage(new ChatComponentText("\247a/consolemod on \247e- toggle consolemod to send commands from \247eWebConsole"));
   		icommandsender.addChatMessage(new ChatComponentText("\247a/consolemod off \247e- toggle consolemod to stop send and recive \247ecommands from WebConsole"));
    	icommandsender.addChatMessage(new ChatComponentText("\247a/consolemod checkserver \247e- check server work \247e(online or \247eoffline)"));
		icommandsender.addChatMessage(new ChatComponentText("\247aAliases: \247e/consolemod //consolemod /conmod //conmod"));
      return;
    }
    icommandsender.addChatMessage(new ChatComponentText("Sample: [" + astring[0] + "]"));

  }

  @Override
  public boolean canCommandSenderUseCommand(ICommandSender icommandsender)
  {
    return true;
  }

  @Override
  public List addTabCompletionOptions(ICommandSender icommandsender,
      String[] astring)
  {
    return null;
  }

  @Override
  public boolean isUsernameIndex(String[] astring, int i)
  {
    return false;
  }

  @Override
  public int compareTo(Object o)
  {
    return 0;
  }
}