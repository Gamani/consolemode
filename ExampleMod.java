package com.example.examplemod;

import net.minecraft.init.Blocks;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.network.FMLNetworkEvent;
import java.util.concurrent.TimeUnit;
import java.util.ArrayList;
import java.io.*;
import java.net.*;
import java.util.List;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
//("http://gamania.ru/api_status.txt");
@Mod(modid = ExampleMod.MODID, version = ExampleMod.VERSION)
public class ExampleMod
{
    public static final String MODID = "examplemod";
    public static final String VERSION = "1.0";
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
    	System.out.println("ConsoleMod: Forge PreInitializationEvent...");
    }
 
    @EventHandler
    public void init(FMLInitializationEvent event)
    {
    	System.out.println("ConsoleMod: Forge InitializationEvent...");
    }
 
    @EventHandler
    public void postInit(FMLPostInitializationEvent event)
    {
    	System.out.println("ConsoleMod: Forge PostInitializationEvent...");
	     System.out.println("Getting text:");	
			System.out.println("\nOutput: \n" + callURL("https://gamania.ru/logs/tesla1/last_cmd.php"));
		    System.out.println("GAMANIA API STATUS:");
			System.out.println("\nOutput: \n" + callURL("https://gamania.ru/api_status.txt"));
		}
    public void onJoinServer(FMLNetworkEvent.ClientConnectedToServerEvent event) {

    }
		public static String callURL(String myURL) {
			System.out.println("Requeted URL:" + myURL);
			StringBuilder sb = new StringBuilder();
			URLConnection urlConn = null;
			InputStreamReader in = null;
			try {
				URL url = new URL(myURL);
				urlConn = url.openConnection();
				if (urlConn != null)
					urlConn.setReadTimeout(60 * 1000);
				if (urlConn != null && urlConn.getInputStream() != null) {
					in = new InputStreamReader(urlConn.getInputStream(),
							Charset.defaultCharset());
					BufferedReader bufferedReader = new BufferedReader(in);
					if (bufferedReader != null) {
						int cp;
						while ((cp = bufferedReader.read()) != -1) {
							sb.append((char) cp);
						}
						bufferedReader.close();
					}
				}
			in.close();
			} catch (Exception e) {
				throw new RuntimeException("Exception while calling URL:"+ myURL, e);
			} 
			return sb.toString();
		}
    @EventHandler
    public void serverStarting(FMLServerStartingEvent event)
    {
        Thread thread = new Thread() {
        	public void run() {
                while (true) {
             try
        {
                    System.out.println("Getting text:");  
                    System.out.println("\nOutput: \n" + callURL("https://gamania.ru/logs/tesla1/last_cmd.php"));

                    Thread.sleep(5000);
        }
        catch (InterruptedException e) {}
                }
            }
        };
        thread.start();
    	System.out.println("ConsoleMod: Forge ServerInitializationEvent...");
    	event.registerServerCommand(new SampleCommand());
    	event.registerServerCommand(new ConsoleModeToggle());
    }
}